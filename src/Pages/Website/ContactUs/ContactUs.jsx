import React, { useState } from "react";
import Layout from "../../../Component/Web/Layout/Layout";
import "./scss/contactUs.css";
import ScrollAnimation from "react-animate-on-scroll";
import "animate.css/animate.min.css";
import { Link } from "react-router-dom";
import { Map, Marker } from "pigeon-maps";
import { post } from "../../../services";
import toast, { Toaster } from "react-hot-toast";

export default function ContactUs() {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [title, setTitle] = useState("");
  const [text, setText] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (name !== "" && email !== "" && title !== "" && text !== "") {
      const payload = {
        name: name,
        email: email,
        title: title,
        text: text,
      };
      post("/contactUs/contactsInsertAPI.php", JSON.stringify(payload))
        .then((res) => {})
        .then(function(response) {
          setName("");
          setEmail("");
          setTitle("");
          setText("");
          toast.success("پیام شما با موفقیت ارسال شد");
        });
    } else {
      toast.error("لطفا تمامی فیلدها را پر نمایید");
    }
  };

  return (
    <>
      <Toaster position="top-left" reverseOrder={false} />

      {/* <Tostify /> */}
      <Layout pageTitle={"تماس با ما"}>
        <section
          className="hero-wrap hero-wrap-2 header-hero-side-page"
          style={{ backgroundImage: "URL(/assets/images/bg_1.jpg)" }}
          data-stellar-background-ratio="0.5"
        >
          <div className="overlay"></div>
          <div className="container">
            <div className="row no-gutters slider-text align-items-center justify-content-center">
              <div
                className="col-md-10 text-center d-flex align-items-center justify-content-between"
                dir="rtl"
              >
                <h1 className="bread">تماس با ما</h1>
                <p className="breadcrumbs mt-2">
                  <span className="mr-2 home">
                    <Link to={"/"}>
                      خانه <i className="ion-ios-arrow-back"></i>
                    </Link>
                  </span>{" "}
                  <span>تماس با ما</span>
                </p>
              </div>
            </div>
          </div>
        </section>

        <section class="ftco-section contact-section bg-light">
          <div class="container">
            <div class="row d-flex mb-5 contact-info text-right" dir="rtl">
              <div class="col-md-12 mb-4">
                <h2 class="h4">اطلاعات تماس</h2>
              </div>
              <div class="w-100"></div>
              <div class="col-md-4">
                <p>
                  <span>آدرس:</span> مشهد، بلوار جلال آل احمر، جلال آل احمر 20،
                  پلاک 20، واحد 3
                </p>
              </div>
              <div class="col-md-2">
                <p>
                  <span>تلفن:</span> <a href="tel://1234567920">09157080918</a>
                </p>
              </div>
              <div class="col-md-2">
                <p>
                  <span>تلفن:</span> <a href="tel://1234567920">09151593897</a>
                </p>
              </div>
              <div class="col-md-3">
                <p>
                  <span>ایمیل:</span>{" "}
                  <a href="mailto:info@yoursite.com">info@pintor.com</a>
                </p>
              </div>
            </div>
          </div>
        </section>
        <section class="ftco-section ftco-no-pt ftco-no-pb contact-section">
          <div class="container-wrap">
            <div
              class="row d-flex align-items-stretch no-gutters text-right"
              dir="rtl"
            >
              <div class="col-md-6 p-5 order-md-last">
                <form onSubmit={(e) => handleSubmit(e)}>
                  <div class="form-group">
                    <input
                      type="text"
                      class="form-control"
                      placeholder="نام شما"
                      value={name}
                      onChange={(e) => setName(e.target.value)}
                    />
                  </div>
                  <div class="form-group">
                    <input
                      type="text"
                      class="form-control"
                      placeholder="ایمیل شما"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                    />
                  </div>
                  <div class="form-group">
                    <input
                      type="text"
                      class="form-control"
                      placeholder="موضوع"
                      value={title}
                      onChange={(e) => setTitle(e.target.value)}
                    />
                  </div>
                  <div class="form-group">
                    <textarea
                      cols="30"
                      rows="7"
                      class="form-control"
                      placeholder="متن پیام"
                      value={text}
                      onChange={(e) => setText(e.target.value)}
                    ></textarea>
                  </div>
                  <div class="form-group">
                    <input
                      type="submit"
                      value="ارسال پیام"
                      class="btn btn-primary px-5"
                    />
                  </div>
                </form>
              </div>
              <div class="col-md-6 d-flex align-items-stretch">
                <Map
                  height={580}
                  defaultCenter={[36.32912013850614, 59.53032016754151]}
                  defaultZoom={17}
                >
                  <Marker
                    width={50}
                    anchor={[36.32912013850614, 59.53032016754151]}
                  />
                </Map>
              </div>
            </div>
          </div>
        </section>
      </Layout>
    </>
  );
}
