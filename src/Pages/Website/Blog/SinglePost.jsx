import React, { useState, useEffect } from "react";
import Layout from "../../../Component/Web/Layout/Layout";
import { Link, useParams } from "react-router-dom";
import LeftSide from "./LeftSide";
import { get } from "../../../services";
import Loading from "../../../Component/Loading/Loading";
import Comment from "./Comment/Comment";

export default function SinglePost() {
  const { id } = useParams();
  const [data, setData] = useState("");
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    get("/blog/getBlogAPI.php").then((response) => {
      for (let index = 0; index < response.length; index++) {
        if (response[index].id === id) {
          const element = response[index];
          setData(element);
          setLoading(false);
        }
      }
    });
  }, [id]);

  return (
    <>
      <Layout pageTitle={`خبرگزاری ما | ${data.title}`}>
        {loading ? (
          <div className="col-md-12 my-5">
            <Loading />
          </div>
        ) : (
          <>
            <section
              className="hero-wrap hero-wrap-2 header-hero-side-page"
              style={{ backgroundImage: "URL(/assets/images/bg_1.jpg)" }}
              data-stellar-background-ratio="0.5"
            >
              <div className="overlay"></div>
              <div className="container">
                <div className="row no-gutters slider-text align-items-center justify-content-center">
                  <div
                    className="col-md-10 text-center d-flex align-items-center justify-content-between"
                    dir="rtl"
                  >
                    <h4 className="bread text-right">{data.title}</h4>
                    <p className="breadcrumbs mt-2">
                      <span className="mr-2 home">
                        <Link to={"/"}>
                          خانه <i className="ion-ios-arrow-back"></i>
                        </Link>
                      </span>{" "}
                      <span>خبرگزاری ما</span>
                    </p>
                  </div>
                </div>
              </div>
            </section>

            <section className="ftco-section">
              <div className="container" dir="rtl">
                <div className="row">
                  <div className="col-lg-8 text-right">
                    <h2 className="mb-3">{data.title}</h2>
                    <p>
                      <img
                        src={`/assets/images/blog/${data.image}`}
                        alt=""
                        className="img-fluid"
                      />
                    </p>
                    <p>{data.text}</p>
                    <div className="tag-widget post-tag-container mb-5 mt-5 d-none">
                      <div className="tagcloud">
                        <a href="#" className="tag-cloud-link">
                          Life
                        </a>
                        <a href="#" className="tag-cloud-link">
                          Sport
                        </a>
                        <a href="#" className="tag-cloud-link">
                          Tech
                        </a>
                        <a href="#" className="tag-cloud-link">
                          Travel
                        </a>
                      </div>
                    </div>
                    <Comment />
                  </div>
                  <LeftSide />
                </div>
              </div>
            </section>
          </>
        )}
      </Layout>
    </>
  );
}
