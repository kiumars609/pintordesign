import React from "react";
import { GetBlogAPI } from "./GetBlogAPI";
import { Link } from "react-router-dom";
import { useSplitDataToSingleArray } from "../../../CustomHooks/useSplitDataToSingleArray";

export default function LeftSide() {
  const { data } = GetBlogAPI();

  const queryLastPosts =
    data &&
    data
      .sort((a, b) => (a.id > b.id ? -1 : 1))
      .slice(0, 4)
      .map((item) => {
        // Show Date
        const year = useSplitDataToSingleArray(item.createdAt).query2[0];
        const month = useSplitDataToSingleArray(item.createdAt).query2[1];
        const day = useSplitDataToSingleArray(item.createdAt).query2[2].slice(
          0,
          2
        );
        return (
          <div className="block-21 mb-4 d-flex" key={item.id}>
            <Link
              to={`/single-post/${item.id}`}
              className="blog-img ml-4"
              style={{
                backgroundImage: `URL(/assets/images/blog/${item.image})`,
              }}
            ></Link>
            <div className="text">
              <h3 className="heading">
                <Link to={`/single-post/${item.id}`}>{item.title}</Link>
              </h3>
              <div className="meta text-left pl-3">
                <div>
                  <span className="icon-calendar"></span> {day}/{month}/{year}
                </div>
                <div>
                  <span className="icon-person"></span> Admin
                </div>
              </div>
            </div>
          </div>
        );
      });

  // When we have repetitive data from db, so we can do this:
  const uniq = Object.values(
    data.reduce((r, c) => {
      r[c.category] = c;
      return r;
    }, {})
  );

  const queryCategory =
    uniq &&
    uniq.map((item, index) => {
      return (
        <li key={index}>
          <Link to={`/post-category/${item.category}`}>{item.category}</Link>
        </li>
      );
    });

  return (
    <>
      <div className="col-lg-4 sidebar ">
        <div className="sidebar-box text-right">
          <h3>دسته بندی</h3>
          <ul className="categories">{queryCategory}</ul>
        </div>

        <div className="sidebar-box text-right" dir="rtl">
          <h3>آخرین پست ها</h3>
          {queryLastPosts}
        </div>

        <div className="sidebar-box d-none">
          <h3>Tag Cloud</h3>
          <ul className="tagcloud m-0 p-0">
            <a href="#" className="tag-cloud-link">
              House
            </a>
            <a href="#" className="tag-cloud-link">
              Office
            </a>
            <a href="#" className="tag-cloud-link">
              Land
            </a>
            <a href="#" className="tag-cloud-link">
              Building
            </a>
            <a href="#" className="tag-cloud-link">
              Table
            </a>
            <a href="#" className="tag-cloud-link">
              Intrior
            </a>
            <a href="#" className="tag-cloud-link">
              Exterior
            </a>
          </ul>
        </div>
      </div>
    </>
  );
}
