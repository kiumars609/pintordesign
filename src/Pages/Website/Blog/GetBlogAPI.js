import { useEffect, useState } from "react";
import { get } from "../../../services";

export const GetBlogAPI = () => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    get("/blog/getBlogAPI.php").then((response) => {
      setData(response);
      setLoading(false);
    });
  }, []);
  return { data, loading };
};
