import React, { useEffect, useState } from "react";
import Layout from "../../../Component/Web/Layout/Layout";
import { Link, useParams } from "react-router-dom";
import { get } from "../../../services";
import RightSide from "./RightSide";
import LeftSide from "./LeftSide";

export default function PostCategory() {
  const { category } = useParams();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    get("/blog/getBlogAPI.php").then((response) => {
      setData(response.filter((find) => find.category === category));
      setLoading(false);
    });
  }, [category]);

  return (
    <>
      <Layout>
        <section
          className="hero-wrap hero-wrap-2 header-hero-side-page"
          style={{ backgroundImage: "URL(/assets/images/bg_1.jpg)" }}
          data-stellar-background-ratio="0.5"
        >
          <div className="overlay"></div>
          <div className="container">
            <div className="row no-gutters slider-text align-items-center justify-content-center">
              <div
                className="col-md-10 text-center d-flex align-items-center justify-content-between"
                dir="rtl"
              >
                <h1 className="bread">خبرگزاری ما | {category}</h1>
                <p className="breadcrumbs mt-2">
                  <span className="mr-2 home">
                    <Link to={"/"}>
                      خانه <i className="ion-ios-arrow-back"></i>
                    </Link>
                  </span>{" "}
                  <span>خبرگزاری ما</span>
                </p>
              </div>
            </div>
          </div>
        </section>
        <section className="ftco-section">
          <div className="container" dir="rtl">
            <div className="row">
              <RightSide data={data} loading={loading} />
              <LeftSide />
            </div>
          </div>
        </section>
      </Layout>
    </>
  );
}
