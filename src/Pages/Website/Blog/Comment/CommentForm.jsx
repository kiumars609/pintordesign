import React from "react";

export default function CommentForm() {
  return (
    <>
      <div className="comment-form-wrap pt-5">
        <h3 className="mb-5 h4 font-weight-bold">یک دیدگاه بگذارید</h3>
        <form action="#" className="p-5 bg-light">
          <div className="form-group">
            <label for="name">
              نام <span className="text-danger">*</span>
            </label>
            <input type="text" className="form-control" id="name" disabled />
          </div>
          <div className="form-group">
            <label for="email">
              ایمیل <span className="text-danger">*</span>
            </label>
            <input type="email" className="form-control" id="email" disabled />
          </div>

          <div className="form-group">
            <label for="message">
              دیدگاه <span className="text-danger">*</span>
            </label>
            <textarea
              name=""
              id="message"
              cols="30"
              rows="10"
              className="form-control"
              disabled
            ></textarea>
          </div>
          <div className="form-group">
            <input
              type="submit"
              value="ارسال دیدگاه"
              className="btn px-4 btn-primary"
              disabled
            />
          </div>
        </form>
      </div>
    </>
  );
}
