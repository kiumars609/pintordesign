import React from "react";
import CommentForm from "./CommentForm";

export default function Comment() {
  return (
    <>
      <div className="pt-5 mt-5">
        <h3 className="mb-5 h4 font-weight-bold">0 دیدگاه</h3>
        <ul className="comment-list">
          {/* First Comment */}
          <li className="comment d-none">
            <div className="vcard bio">
              <img src="images/person_1.jpg" alt="Image placeholder" />
            </div>
            <div className="comment-body">
              <h3>John Doe</h3>
              <div className="meta mb-2">January 03, 2019 at 2:21pm</div>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Pariatur quidem laborum necessitatibus, ipsam impedit vitae
                autem, eum officia, fugiat saepe enim sapiente iste iure! Quam
                voluptas earum impedit necessitatibus, nihil?
              </p>
              <p>
                <a href="#" className="reply">
                  Reply
                </a>
              </p>
            </div>
          </li>
          {/*  */}
        </ul>

        <CommentForm />
      </div>
    </>
  );
}
