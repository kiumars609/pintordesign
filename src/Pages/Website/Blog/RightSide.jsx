import React from "react";
import Loading from "../../../Component/Loading";
import { useSplitDataToSingleArray } from "../../../CustomHooks/useSplitDataToSingleArray";
import { ChangeDataPaginate } from "../../../CustomHooks/Pagination/ChangeDataPaginate";
import { Pagination } from "../../../CustomHooks/Pagination/Pagination";
import { Link } from "react-router-dom";

export default function RightSide({ data, loading }) {

  console.log(data);
  // Pagination Operation
  const {
    currentPosts,
    postsPerPage,
    currentPage,
    paginate,
  } = ChangeDataPaginate({ perPageShow: 8, data });

  const queryData =
    currentPosts &&
    currentPosts.map((item) => {
      // Show Date
      const year = useSplitDataToSingleArray(item.createdAt).query2[0];
      const month = useSplitDataToSingleArray(item.createdAt).query2[1];
      const day = useSplitDataToSingleArray(item.createdAt).query2[2].slice(
        0,
        2
      );
      return (
        <div className="col-md-6" key={item.id}>
          <div className="blog-entry">
            <Link
              to={`/single-post/${item.id}`}
              className="block-20"
              style={{
                backgroundImage: `URL(/assets/images/blog/${item.image})`,
              }}
            ></Link>
            <div className="text d-flex py-4" dir="ltr">
              <div className="meta mb-3 text-left">
                <div>
                  {day}/{month}/{year}
                </div>
                <div>Admin</div>
              </div>
              <div className="desc pl-3 text-right">
                <h3 className="heading">
                  <Link to={`/single-post/${item.id}`}>{item.title}</Link>
                </h3>
              </div>
            </div>
          </div>
        </div>
      );
    });
  return (
    <>
      <div className="col-lg-8">
        {loading ? (
          <Loading />
        ) : (
          <>
            <div className="row">{queryData}</div>
            <div className="row no-gutters my-5">
              <div className="col text-center">
                <div className="block-27">
                  <Pagination
                    postsPerPage={postsPerPage}
                    totalPosts={data.length}
                    paginate={paginate}
                    currentPage={currentPage}
                  />
                </div>
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
}
