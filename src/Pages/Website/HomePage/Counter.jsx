import React from "react";

export default function Counter() {
  return (
    <>
      <section
        className="ftco-section ftco-counter img mt-5"
        id="section-counter"
        style={{ backgroundImage: "URL(/assets/images/bg_3.jpg)" }}
        data-stellar-background-ratio="0.5"
      >
        <div className="container">
          <div className="d-flex align-items-center justify-content-center">
            <div className="col-lg-10">
              <div className="row d-md-flex align-items-center">
                <div className="col-md d-flex justify-content-center counter-wrap ">
                  <div className="block-18">
                    <div className="text">
                      <strong className="number" data-number="18">
                        5
                      </strong>
                      <span>سال تجربه</span>
                    </div>
                  </div>
                </div>
                <div className="col-md d-flex justify-content-center counter-wrap ">
                  <div className="block-18">
                    <div className="text">
                      <strong className="number" data-number="351">
                        32
                      </strong>
                      <span>تعداد مشتریان</span>
                    </div>
                  </div>
                </div>
                <div className="col-md d-flex justify-content-center counter-wrap ">
                  <div className="block-18">
                    <div className="text">
                      <strong className="number" data-number="564">
                        34
                      </strong>
                      <span>پروژه های به اتمام رسیده</span>
                    </div>
                  </div>
                </div>
                <div className="col-md d-flex justify-content-center counter-wrap ">
                  <div className="block-18">
                    <div className="text">
                      <strong className="number" data-number="300">
                        2
                      </strong>
                      <span>پروژه های درحال ساخت</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
