import React, { useState } from "react";
import { Link } from "react-router-dom";

export default function Projects() {
  const [sort, setSort] = useState("");

  const data = [
    {
      category: "interior",
      categoryFa: "داخلی",
      title: "پروژه صیاد شیرازی",
      image: "sayad/1.jpg",
    },
    {
      category: "exterior",
      categoryFa: "خارجی",
      title: "پروژه ملاصدرا",
      image: "/work-2.jpg",
    },
    {
      category: "landscape",
      categoryFa: "چشم انداز",
      title: "پروژه جلال",
      image: "/work-10.jpg",
    },
    {
      category: "interior",
      categoryFa: "داخلی",
      title: "پروژه زنبق",
      image: "/work-12.jpg",
    },
    {
      category: "interior",
      categoryFa: "داخلی",
      title: "پروژه 17 شهریور",
      image: "/work-4.jpg",
    },
    {
      category: "interior",
      categoryFa: "داخلی",
      title: "پروژه بیمارستان مهر",
      image: "/work-6.jpg",
    },
    {
      category: "interior",
      categoryFa: "داخلی",
      title: "پروژه حسابی",
      image: "/work-9.jpg",
    },
    {
      category: "interior",
      categoryFa: "داخلی",
      title: "پروژه 7 تیر",
      image: "/work-11.jpg",
    },
  ];

  const handleSort = (value) => {
    setSort(value);
  };

  const queryData =
    data &&
    data.map((item, index) => {
      if (sort !== "") {
        if (item.category === sort) {
          return (
            <div className="col-md-6 col-lg-3" key={index}>
              <div className="project">
                <img
                  src={`/assets/images/projects/${item.image}`}
                  className="img-fluid"
                  alt={item.title}
                  style={{ width: "100%", height: "335px", objectFit: "cover" }}
                />
                <div className="text">
                  <span>{item.categoryFa}</span>
                  <h3>
                    <a href="project.html">{item.title}</a>
                  </h3>
                </div>
                <a
                  href="images/work-1.jpg"
                  className="icon image-popup d-flex justify-content-center align-items-center"
                >
                  <span className="icon-expand"></span>
                </a>
              </div>
            </div>
          );
        }
      } else {
        return (
          <div className="col-md-6 col-lg-3" key={index}>
            <div className="project">
              <img
                src={`/assets/images/projects/${item.image}`}
                className="img-fluid"
                alt={item.title}
                style={{ width: "100%", height: "335px", objectFit: "cover" }}
              />
              <div className="text">
                <span>{item.categoryFa}</span>
                <h3>
                  <a href="project.html">{item.title}</a>
                </h3>
              </div>
              <a
                href="images/work-1.jpg"
                className="icon image-popup d-flex justify-content-center align-items-center"
              >
                <span className="icon-expand"></span>
              </a>
            </div>
          </div>
        );
      }
    });

  return (
    <>
      <section className="ftco-section ftco-no-pb ftco-projects">
        <div className="container">
          <div className="d-flex header mb-5 pb-2">
            <div className="col-md-4 heading-section ">
              <span className="subheading subheading-with-line">
                <small className="pr-2 bg-white">پروژه ها</small>
              </span>
              <h2 className="mb-4">پروژه های شرکت پینتور</h2>
            </div>
            <div className="col-md-8 pl-md-5 heading-section ">
              <div className="pl-md-4 border-line">
                <p className="text-right" dir="rtl">
                  در این بخش می توانید به سادگی از جدیدترین پروژه های شرکت
                  معماری و دکوراسیون داخلی پینتور دیدن کنید.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div
          className="col-md-12 d-flex justify-content-center category mb-5 text-right"
          dir="rtl"
        >
          <span className="option mr-2">
            <span onClick={(e) => handleSort("")}>تمام پروژه ها</span>
          </span>
          <span className="option mr-3">
            <span onClick={(e) => handleSort("interior")}>داخلی</span>
          </span>
          <span className="option mr-3">
            <span onClick={(e) => handleSort("exterior")}>خارجی</span>
          </span>
          <span className="option mr-3">
            <span onClick={(e) => handleSort("landscape")}>چشم انداز</span>
          </span>
        </div>
        <div className="container-wrap">
          <div className="row no-gutters">{queryData}</div>
        </div>
        <div className="col-md-12 d-flex justify-content-center my-3">
          <Link to={"/projects"} class="btn-custom fs-5">
            مشاهده همه پروژه ها
          </Link>
        </div>
      </section>
    </>
  );
}
