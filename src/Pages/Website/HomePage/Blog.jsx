import React from "react";
import ScrollAnimation from "react-animate-on-scroll";
import "animate.css/animate.min.css";
import { Link } from "react-router-dom";
import { useSplitDataToSingleArray } from "../../../CustomHooks/useSplitDataToSingleArray";

export default function Blog({ data, loading }) {
  const queryData =
    data &&
    data
      .sort((a, b) => (a.id > b.id ? -1 : 1))
      .slice(0, 4)
      .map((item) => {
        // Show Date
        const year = useSplitDataToSingleArray(item.createdAt).query2[0];
        const month = useSplitDataToSingleArray(item.createdAt).query2[1];
        const day = useSplitDataToSingleArray(item.createdAt).query2[2].slice(
          0,
          2
        );
        return (
          <ScrollAnimation
            animateIn="flipInY"
            animateOut="flipOutY"
            className="col-md-4"
            key={item.id}
          >
            <div className="blog-entry">
              <Link
                className="block-20 image"
                to={`/single-post/${item.id}`}
                style={{
                  backgroundImage: `URL(/assets/images/blog/${item.image})`,
                }}
              ></Link>

              <div className="text d-flex pt-2 flex-column col-md-12 px-0">
                
                <div className="col-md-12 text-right" dir="rtl">
                  <h3 className="heading">
                    <Link to={`/single-post/${item.id}`}>{item.title}</Link>
                  </h3>
                </div>
                <div className="mb-1 col-md-12 text-right">
                  <div>
                    <span>
                      {day}/{month}/{year}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </ScrollAnimation>
        );
      });

  return (
    <>
      <section className="ftco-section ftco-blog">
        <div className="container">
          <div className="row justify-content-start mb-5 pb-2">
            <div className="col-md-4 heading-section ">
              <span className="subheading subheading-with-line">
                <small className="pr-2 bg-white">خبرنامه</small>
              </span>
              <h2 className="mb-4">آخرین خبرنامه</h2>
            </div>
            <div className="col-md-8 pl-md-5 heading-section ">
              <div className="pl-md-4 border-line text-right" dir="rtl">
                <p>آخرین و جدیدترین خبرگذاری های معماری را از ما بخواهید.</p>
              </div>
            </div>
          </div>
          <div className="row">
            {queryData}
            <div className="col-md-12 d-flex justify-content-center my-3">
              <Link to={"/blog"} class="btn-custom fs-5">
                مشاهده همه خبرنامه ها
              </Link>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
