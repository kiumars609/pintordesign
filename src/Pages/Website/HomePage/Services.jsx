import React from "react";
import ScrollAnimation from "react-animate-on-scroll";
import "animate.css/animate.min.css";

export default function Services() {
  return (
    <>
      <section className="ftco-services">
        <div className="container">
          <div className="row justify-content-start mb-5 pb-5">
            <div className="col-md-4 heading-section ">
              <h2 className="mb-4">جریان کار ما</h2>
            </div>
            <div className="col-md-8 pr-md-5 ">
              <div className="pr-md-4 border-line text-right" dir="rtl">
                <p>مراحل انجام پروژه در شرکت پینتور</p>
              </div>
            </div>
          </div>
          <div className="row">
            <ScrollAnimation
              animateIn="flipInY"
              animateOut="flipOutY"
              className="col-md-3 d-flex align-self-stretch "
            >
              <div className="media block-6 services d-block">
                <div className="icon d-flex justify-content-center align-items-center">
                  <span className="flaticon-idea"></span>
                </div>
                <div className="media-body p-2 mt-3">
                  <h3 className="heading">مشاور تلفنی رایگان</h3>
                  <p>
                    در مرحله ی اول کار شما می توانید با شرکت پینتور و با
                    کارشناسان شرکت ما در تماس باشید؛ این کار را به کمک بخش
                    نارنجی رنگی که در پایین صفحه مشاهده می کنید می توانید شروع
                    کنید و یا شماره تلفن های شرکت را بدست آورید و با ما تماس
                    بگیرید.
                  </p>
                </div>
              </div>
            </ScrollAnimation>

            <ScrollAnimation
              animateIn="flipInY"
              animateOut="flipOutY"
              className="col-md-3 d-flex align-self-stretch "
            >
              <div className="media block-6 services d-block">
                <div className="icon d-flex justify-content-center align-items-center">
                  <span className="flaticon-compass-symbol"></span>
                </div>
                <div className="media-body p-2 mt-3">
                  <h3 className="heading">بازدید رایگان</h3>
                  <p>
                    در مرحله ی بازدید شرکت پینتور باز هم مانند مشاوره ی رایگان
                    تلفنی خود به صورت رایگان خدمات خود را ارائه خواهد داد.خدماتی
                    که در ارتباط با آن ها می توانید با شرکت پینتور شروع کنید اعم
                    از: بازسازی ساختمان، بازسازی مسکونی، بازسازی ویلا، بازسازی
                    اداری و ...
                  </p>
                </div>
              </div>
            </ScrollAnimation>

            <ScrollAnimation
              animateIn="flipInY"
              animateOut="flipOutY"
              className="col-md-3 d-flex align-self-stretch "
            >
              <div className="media block-6 services d-block">
                <div className="icon d-flex justify-content-center align-items-center">
                  <span className="flaticon-layers"></span>
                </div>
                <div className="media-body p-2 mt-3">
                  <h3 className="heading">عقد قرارداد</h3>
                  <p>
                    بعد از جلسات رایگان و بازدیدهای رایگانی که برای شما در شرکت
                    پینتور در نظر گرفته می شود حالا به مرحله ی عقد قرارداد می
                    رسیم تا بالاخره به بررسی جهت های مختلف پروژه و لیست هزینه ها
                    بپردازیم.در این مرحله پیش فاکتور صادر می شود و قرارداد بسته
                    می شود نکته ای که در این بخش داریم این است که باید دقت کنید
                    پروژه های ساختمانی با توجه به اوضاع اقتصادی به صورت روزانه
                    فاکتور می شوند و مصالح بعد از خرید به مبلغ پروژه اضافه می
                    شوند.
                  </p>
                </div>
              </div>
            </ScrollAnimation>

            <ScrollAnimation
              animateIn="flipInY"
              animateOut="flipOutY"
              className="col-md-3 d-flex align-self-stretch "
            >
              <div className="media block-6 services d-block">
                <div className="icon d-flex justify-content-center align-items-center">
                  <span className="flaticon-layers"></span>
                </div>
                <div className="media-body p-2 mt-3">
                  <h3 className="heading">اجرای پروژه</h3>
                  <p>
                    در مرحله ی اجرای پروژه تیم پینتور مثل مراحل قبلی به صورت
                    حرفه ای خدمات خود را ارائه می دهد و حالا شما می توانید
                    بهترین خدمات ساختمانی را که در مشهد می باشد مشاهده کنید و از
                    خدمات خود لذت ببرید.شرکت پینتور در پایان عملیات اجرا از
                    مشتریان خود نظرسنجی انجام می دهد و از آن ها اجازه ی انتشار
                    مراحل پروژه ی شما را دریافت می کند.
                  </p>
                </div>
              </div>
            </ScrollAnimation>
          </div>
          <div className="row justify-content-start mt-5 py-5">
            <div className="col-md-4 heading-section ">
              <h2 className="mb-4">خدمات ما</h2>
              <p dir="rtl" className="text-right">
                خدمات اصلی شرکت معماری و دکوراسیون داخلی پینتور
              </p>
            </div>
            <div className="col-md-8 ">
              <div className="row d-flex">
                <ScrollAnimation
                  animateIn="flipInY"
                  animateOut="flipOutY"
                  className="col-md-6 d-flex align-items-stretch"
                >
                  <div className="media block-6 services services-2 d-block bg-light p-4 mb-4">
                    <div className="icon d-flex justify-content-center align-items-center">
                      <span className="flaticon-layers"></span>
                    </div>
                    <div className="media-body p-2 mt-3">
                      <h3 className="heading">دکوراسیون داخلی</h3>
                      <p>
                        دکوراسیون داخلی در حقیقت به معنای خلق یک فضاست که در عین
                        زیبایی و منحصر به فرد بودن آن کاربری های لازم را داشته
                        باشد. دکوراسیون داخلی از متریال ها و المان های متفاوتی
                        تشکیل شده است که توجه به هر کدام از این المان ها و
                        متریال ها از اهمیت ویژه ای برخوردار می باشد. مهمترین
                        نکته در دکوراسیون داخلی هماهنگی و هارمونی بین این اجزا
                        می باشد که با رعایت این هماهنگی و هارمونی که در مجموع
                        مقالات دکوطرح به آن پرداخته شده است یک دکوراسیون دلنشین
                        و آرامش بخش خلق می شود. یک دکوراسیون داخلی زیبا و چشم
                        نواز می تواند با کمترین هزینه ممکن شود به شرط آنکه اصول
                        معماری و کمی ذوق و ایده پردازی در آن دخیل باشد.
                      </p>
                    </div>
                  </div>
                </ScrollAnimation>

                <ScrollAnimation
                  animateIn="flipInY"
                  animateOut="flipOutY"
                  className="col-md-6 d-flex align-items-stretch"
                >
                  <div className="media block-6 services services-2 d-block bg-light p-4 mb-4">
                    <div className="icon d-flex justify-content-center align-items-center">
                      <span className="flaticon-layers"></span>
                    </div>
                    <div className="media-body p-2 mt-3">
                      <h3 className="heading">بازسازی ساختمان</h3>
                      <p>
                        بازسازی ساختمان به مجموعه عملیات و اقدامات مرمتی که شخص
                        و یا گروهی بر روی یک ساختمان و بنا انجام میدهند تا عمر
                        مفید آن بنا و ساختمان را افزایش دهند و بتوانند استفاده
                        بهینه تری از آن بنا داشته باشند. بازسازی ساختمان از
                        قوانین و قواعد خاصی پیروی می کند و مهمترین چیزی که این
                        قواعد و قوانین را دستخوش تغییرمی کند محل و فضایی است که
                        می خواهید در آن عملیات بازسازی انجام دهید. در بازسازی
                        ساختمان شما می توانید ساختارهای جدیدی را جاگزین
                        ساختارهای قدیمی کنید که این تغیرات در ساختارها می تواند
                        باعث افزایش قیمت ملک ، بالا رفتن کیفیت زندگی ساکنین
                        ساختمان، بهینه سازی مصرف انرژی، تقویت سازه ساختمان و
                        رفاه بیشتر ساکنین شود.
                      </p>
                    </div>
                  </div>
                </ScrollAnimation>
                <ScrollAnimation
                  animateIn="flipInY"
                  animateOut="flipOutY"
                  className="col-md-6 d-flex align-items-stretch"
                >
                  <div className="media block-6 services services-2 d-block bg-light p-4 mb-4">
                    <div className="icon d-flex justify-content-center align-items-center">
                      <span className="flaticon-layers"></span>
                    </div>
                    <div className="media-body p-2 mt-3">
                      <h3 className="heading">ساخت ساختمان</h3>
                      <p>
                        همه انسان ها دوست دارند که درمحیط و فضایی آرام و امن
                        زندگی کنند و دوست دارند جا و مکانی را که به عنوان زندگی
                        انتخاب می کنند کاملا ساخته شده باشد. شاد این سوال ذهن
                        بسیاری از ما را مشغول خود کند که مراحل ساخت ساختمان چیست
                        و چه مراحلی باید طی شود تا یک ساختمان مورد بهره برداری
                        قرار گیرد. ساخت ساختمان دارای ده مرحله می باشد که این
                        مراحل شامل گود برداری، اجرای پی، اجرای ستون ها، اجرای
                        سقف، دیوار چینی، کف سازی، تاسیسات مکانیکی و برق، نازک
                        کاری، اجرای نما و اجرای محوطه می باشد که هر کدام از این
                        مراحل به تفکیک در بخش مقالات دکوطرح توضیح داده شده است.
                      </p>
                    </div>
                  </div>
                </ScrollAnimation>
                <ScrollAnimation
                  animateIn="flipInY"
                  animateOut="flipOutY"
                  className="col-md-6 d-flex align-items-stretch"
                >
                  <div className="media block-6 services services-2 d-block bg-light p-4 mb-4">
                    <div className="icon d-flex justify-content-center align-items-center">
                      <span className="flaticon-layers"></span>
                    </div>
                    <div className="media-body p-2 mt-3">
                      <h3 className="heading">بازسازی منزل</h3>
                      <p>
                        هنگامی که شما تصمیم می‌گیرید که منزل و آپارتمانی را
                        بازسازی کنید باید میزان فرسودگی آن را حساب کنید و همچنین
                        مکان‌هایی که نیازمند تعمیر هستند را شناسایی کنید. همین
                        موضوع باعث می‌شود کسانی که می‌خواهند بازسازی کنند کارشان
                        راحت تر شود و نوع دکوراسیونی که می خواهند استفاده کنند
                        هم مورد بررسی قرار می‌گیرد. در ابتدای کار برای بازسازی
                        خانه باید به یک متخصص خبره که در این زمینه فعالیت می‌کند
                        رجوع کنید. این فرد متخصص در زمینه بازسازی‌ منزل همه
                        موارد را باید مورد بررسی قرار دهد و آنها را تعمیر کند.
                        از طرفی منزل‌هایی که بسیار‌ قدیمی و فرسوده می باشند باید
                        بازسازی شوند زیرا ممکن است طی یک اتفاق ناگوار خانه دچار
                        ویرانی و خرابی شدید شود و قسمت‌هایی که فرسوده تر هستند
                        عواقب جبران ناپذیری را به وجود بیاورند.
                      </p>
                    </div>
                  </div>
                </ScrollAnimation>

                <ScrollAnimation
                  animateIn="flipInY"
                  animateOut="flipOutY"
                  className="col-md-6 d-flex align-items-stretch"
                >
                  <div className="media block-6 services services-2 d-block bg-light p-4 mb-4">
                    <div className="icon d-flex justify-content-center align-items-center">
                      <span className="flaticon-layers"></span>
                    </div>
                    <div className="media-body p-2 mt-3">
                      <h3 className="heading">بازسازی ویلا</h3>
                      <p>
                        برای محیط ویلا با هر متراژی که باشد یک سری استانداردها
                        وجود دارد که میتوانید از آنها استفاده کنید. اولین مورد
                        از بازسازی ویلا که بسیار اهمیت دارد نور مناسب است. نور
                        از نکات مهم در طراحی و بازسازی می‌باشد. اگر قسمتی از
                        ویلا وجود داشته باشد که نور نیم سوز یا کم نور دارد با
                        تعویض آن میتوانید یک جلوه جدیدی را وارد محیط منزل کنید.
                        همانطور که میدانید وجود نور مناسب میتواند تاثیر چشم گیری
                        در روحیه افراد بگذارد. برای مثال تصور کنید که در تراس
                        ویلا نشسته اید و با نور کم دور یکدیگر جمع شده اید. پس از
                        مدتی ممکن است که چشمتان خسته شود و جمع را ترک کنید و به
                        قسمتی بروید که نور مناسبی دارد. اما با یک هزینه ناچیز
                        میتوانید آن محیط را پر نور کنید. حتی استفاده از
                        نورپردازی های امروزی هم میتوانند به زیبایی ویلا
                        بیافزایند.
                      </p>
                    </div>
                  </div>
                </ScrollAnimation>

                <ScrollAnimation
                  animateIn="flipInY"
                  animateOut="flipOutY"
                  className="col-md-6 d-flex align-items-stretch"
                >
                  <div className="media block-6 services services-2 d-block bg-light p-4 mb-4">
                    <div className="icon d-flex justify-content-center align-items-center">
                      <span className="flaticon-layers"></span>
                    </div>
                    <div className="media-body p-2 mt-3">
                      <h3 className="heading">بازسازی خانه</h3>
                      <p>
                        عملیات بازسازی خانه یکی از پراهمیت ترین خدمات دکوطرح می
                        باشد. تجربه ما طی سالیان سال بدست آمده است. خدمات ما از
                        مرحله ی طراحی کار و پرداخت پیش پرداخت و خرید مصالح شروع
                        می شود و تا تحویل و ارائه به کارفرما ادامه دارد. در طول
                        انجام مراحل بازسازی می توانید از پیشرفت کار در حین کار
                        با خبر باشید و نظرات سازنده ی خود را به تیم ما بدهید.
                        عملیات بازسازی اغلب در دو مرحله ی تجاری و یا مسکونی رخ
                        خواهد داد. در فرآیند های اجتماعی، گاهی نوسازی، به انجام
                        خدمات نو آورانه یا خلق اثری جدید می رسد. مثلا گاهی
                        بازسازی یک اجتماع، به تقویت شدن و در نهایت احیای آن ختم
                        خواهد شد. تیم دکوطرح در تمامی مراحل بازسازی منزل یا
                        فرآیند بازسازی خانه ی شما همراه شما خواهد بود و ضمانت
                        تجربه بی‌نظیر از بازسازی را به شما می‌دهد.
                      </p>
                    </div>
                  </div>
                </ScrollAnimation>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
