import React from "react";
import { Link } from "react-router-dom";
import ScrollAnimation from "react-animate-on-scroll";
import "animate.css/animate.min.css";

export default function AboutUs() {
  return (
    <>
      <section class="ftco-section ftc-no-pb ftco-about-us">
        <div class="container">
          <div class="no-gutters d-flex">
            <ScrollAnimation
              animateIn="bounceInLeft"
              animateOut="bounceOutRight"
              className="col-md-5 p-md-5 img img-2"
              style={{ backgroundImage: "URL(/assets/images/bg_3.jpg)" }}
            ></ScrollAnimation>

            <div class="col-md-7 wrap-about pb-md-5">
              <div class="heading-section mb-5 pl-md-5">
                <div class="pl-md-5 ml-md-5">
                  <span class="subheading subheading-with-line">
                    <small class="pl-2 bg-white">درباره ما</small>
                  </span>
                  <h2 class="mb-4">
                    درباره شرکت معماری و دکوراسیون داخلی پینتور
                  </h2>
                </div>
              </div>
              <ScrollAnimation
                animateIn="bounceInRight"
                animateOut="bounceOutLeft"
              >
                <div class="pl-md-5 ml-md-5 mb-5 text-right">
                  <p dir="rtl" className="describe">
                    از مهمترین موضوع هایی که در یک شرکت طراحی دکوراسیون داخلی
                    بسیار مهم است جلب رضایت مشتری و کارفرمامی باشد هنگامی که در
                    محیطی زندگی می کنید باید علاوه بر زیبایی حس امنیت و آرامش را
                    طراحی کنید، که این عمل به وسیله شرکت های دکوراسیون داخلی
                    محقق می شود. در شرکت‌های دکوراسیون داخلی با به کار گیری
                    نیروهای مجرب و ماهر در این زمینه فعالیت های خود را شروع می
                    کنند که با مراجعه به آنها این امکان برای شما میسر می شود
                    برای طراحی داخلی منزل با کاربری های مختلف مانند مسکونی ،
                    اداری ، ویلا ، تجاری و غیره مراجعه نمایید.
                  </p>
                  <p dir="rtl">
                    <Link to={"/about-us"} class="btn-custom">
                      بیشتر <span class="ion-ios-arrow-back"></span>
                    </Link>
                  </p>
                </div>
              </ScrollAnimation>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
