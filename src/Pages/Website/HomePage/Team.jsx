import React from "react";
import ScrollAnimation from "react-animate-on-scroll";
import "animate.css/animate.min.css";

export default function Team() {
  return (
    <>
      <section className="ftco-section">
        <div className="container">
          <div className="row justify-content-center mb-5 pb-3">
            <div className="col-md-7 heading-section ">
              <span className="subheading subheading-with-line">
                <small className="pl-2 bg-white">تیم حرفه ای</small>
              </span>
              <h2 className="mb-4">تیم ما</h2>
              <p className="d-none">
                Far far away, behind the word mountains, far from the countries
                Vokalia and Consonantia, there live the blind texts. Separated
                they live in
              </p>
            </div>
          </div>
          <div className="d-flex justify-content-center flex-wrap">
            <div className="col-md-6 col-lg-3 ">
              <ScrollAnimation
                animateIn="bounceInLeft"
                animateOut="bounceOutRight"
              >
                <div className="staff">
                  <div
                    className="img"
                    style={{
                      backgroundImage: "URL(/assets/images/icon/architect.png)",
                    }}
                  ></div>
                  <div className="text px-4 pt-4">
                    <h3>مهندس مهدخت اجرتی</h3>
                    <span className="position mb-2">
                      هم بنیانگذار / مدیر عامل
                    </span>
                    <div className="faded">
                      <p>
                        I am an ambitious workaholic, but apart from that,
                        pretty simple person.
                      </p>
                      <ul className="ftco-social d-flex">
                        <li className="">
                          <a
                            href="https://instagram.com/pintor.design.official"
                            target="_blank"
                          >
                            <span className="icon-instagram"></span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </ScrollAnimation>
            </div>

            <div className="col-md-6 col-lg-3">
              <ScrollAnimation
                animateIn="bounceInRight"
                animateOut="bounceOutLeft"
              >
                <div className="staff">
                  <div
                    className="img"
                    style={{
                      backgroundImage: "URL(/assets/images/icon/architect.png)",
                    }}
                  ></div>
                  <div className="text px-4 pt-4">
                    <h3>مهندس نگار اکبری</h3>
                    <span className="position mb-2">
                      هم بنیانگذار / مدیر عامل
                    </span>
                    <div className="faded">
                      <p>
                        I am an ambitious workaholic, but apart from that,
                        pretty simple person.
                      </p>
                      <ul className="ftco-social d-flex">
                        <li className="">
                          <a
                            href="https://instagram.com/pintor.design"
                            target="_blank"
                          >
                            <span className="icon-instagram"></span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </ScrollAnimation>
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
