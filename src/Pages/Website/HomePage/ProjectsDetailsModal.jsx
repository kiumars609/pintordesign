import React from "react";

export default function ProjectsDetailsModal({ data }) {
  return (
    <>
      <div
        className="modal fade"
        id={`exampleModal${data.id}`}
        tabindex="-1"
        aria-labelledby={`exampleModalLabel${data.id}`}
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered modal-xl">
          <div className="modal-content">
            <div className="modal-header" dir="rtl">
              <h3 className="modal-title" id={`exampleModalLabel${data.id}`}>
                {data.title}
              </h3>
              <button
                type="button"
                className="btn-close"
                data-bs-dismiss="modal"
                aria-label="Close"
              ></button>
            </div>
            <div className="modal-body p-0">
              <div className="col-md-12 d-flex px-0">
                {/* Start Left Side (Slider) */}
                <div className="col-md-8 px-0">
                  <div
                    id={`carouselExampleControls${data.id}`}
                    className="carousel slide"
                    data-bs-ride="carousel"
                  >
                    <div className="carousel-inner">
                      <div className="carousel-item active">
                        <img
                          src={`/assets/images/projects/${data.image}`}
                          className="d-block w-100"
                          alt={data.title}
                        />
                      </div>
                      <div className="carousel-item">
                        <img
                          src={`/assets/images/projects/${data.image2}`}
                          className="d-block w-100"
                          alt={data.title}
                        />
                      </div>
                      <div className="carousel-item">
                        <img
                          src={`/assets/images/projects/${data.image3}`}
                          className="d-block w-100"
                          alt={data.title}
                        />
                      </div>
                    </div>
                    <button
                      className="carousel-control-prev"
                      type="button"
                      data-bs-target={`#carouselExampleControls${data.id}`}
                      data-bs-slide="prev"
                    >
                      <span
                        className="carousel-control-prev-icon"
                        aria-hidden="true"
                      ></span>
                      <span className="visually-hidden">Previous</span>
                    </button>
                    <button
                      className="carousel-control-next"
                      type="button"
                      data-bs-target={`#carouselExampleControls${data.id}`}
                      data-bs-slide="next"
                    >
                      <span
                        className="carousel-control-next-icon"
                        aria-hidden="true"
                      ></span>
                      <span className="visually-hidden">Next</span>
                    </button>
                  </div>
                </div>
                {/* End Left Side (Slider) */}

                {/* Start Right Side (Text) */}
                <div className="col-md-4 p-4 text-right" dir="rtl">
                  {data.text}
                </div>
                {/* End Right Side (Text) */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
