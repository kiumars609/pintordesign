import React from "react";
import "./scss/homePage.css";
import Layout from "../../../Component/Web/Layout/Layout";
import Slider from "./Slider/Slider";
import Team from "./Team";
import AboutUs from "./AboutUs";
import Counter from "./Counter";
import Services from "./Services";
import Projects from "./Projects";
import Clients from "./Clients";
import Blog from "./Blog";
import FrequentlyQuestions from "./FrequentlyQuestions";
import { GetBlogAPI } from "../Blog/GetBlogAPI";

export default function HomePage() {
  const { data, loading } = GetBlogAPI();

  return (
    <>
      <Layout pageTitle={"Home Page"}>
        <Slider />
        <AboutUs />
        <Counter />
        <Services />
        <FrequentlyQuestions />
        <Projects />
        <Team />
        <Blog data={data} loading={loading} />
        <Clients />
      </Layout>
    </>
  );
}
