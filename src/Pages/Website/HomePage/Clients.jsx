import React from "react";
import CarouselProduct from "../../../Component/CarouselProduct/CarouselProduct";
import ScrollAnimation from "react-animate-on-scroll";
import "animate.css/animate.min.css";

export default function Clients() {
  return (
    <>
      <section className="ftco-section ftco-client">
        <div className="container">
          <div className="row justify-content-start mb-5 pb-2">
            <div className="col-md-4 heading-section ">
              <span className="subheading subheading-with-line">
                <small className="pr-2 bg-white">مشتریان</small>
              </span>
              <h2 className="mb-4">مشتریان ما</h2>
            </div>
            <div className="col-md-8 pl-md-5 heading-section ">
              <div className="pl-md-4 border-line text-right">
                <p>ما مشتری های خاص و وفاداری داریم.</p>
              </div>
            </div>
          </div>
          <div className="col-md-12">
            {/* <CarouselProduct /> */}

            <ScrollAnimation
              animateIn="bounceInRight"
              animateOut="bounceOutLeft"
            >
              <section className="">
                <div className="container overflow-hidden">
                  <div className="row gy-5 gy-md-6">
                    <div className="col-6 col-md-3 align-self-center text-center">
                      <a href="https://mehr.pet/" target="_blank">
                        <img
                          src="/assets/images/clients/mehr-pet-logo.png"
                          className="col-md-12"
                        />
                      </a>
                    </div>
                  </div>
                </div>
              </section>
            </ScrollAnimation>
          </div>
        </div>
      </section>
    </>
  );
}
