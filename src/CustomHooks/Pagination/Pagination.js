import React from "react";

export const Pagination = ({
  postsPerPage,
  totalPosts,
  paginate,
  currentPage,
}) => {
  const pageNumbers = [];

  for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
    pageNumbers.push(i);
  }

  return (
    <>
      {/* You can here custom your pagination number */}
      <ul>
        {pageNumbers.map((number) => (
          <li key={number} role="button" className={`${number === currentPage && "active"} ml-2`}>
            <a onClick={() => paginate(number)}>{number}</a>
          </li>
        ))}
      </ul>
    </>
  );
};
