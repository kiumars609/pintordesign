import { useState } from "react";

// ******* data is your get data's from db (axios) *******
// *******  In Page you want to add Pagination.js component for show number of page  *******
// ******* you most use currentPosts instate of normal data that get from db (axios) (that's mean use currentPosts in map) *******
// ******* in <Pagination /> for totalPosts, you most use data that get from db (axios) *******

export const ChangeDataPaginate = ({ perPageShow = 5, data }) => {
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerPage] = useState(perPageShow);

  // Get Current Posts
  const indexOfLastPost = currentPage * postsPerPage;
  const indexOfFirstPost = indexOfLastPost - postsPerPage;
  const currentPosts = data.slice(indexOfFirstPost, indexOfLastPost);

  //   Change Page (after click for onClick in Pagination.js)
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  // use this
  // <Pagination
  //   postsPerPage={postsPerPage}
  //   totalPosts={data.length}
  //   paginate={paginate}
  // />;

  return { currentPosts, postsPerPage, currentPage, paginate };
};
