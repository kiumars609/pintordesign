import React, { Component } from "react";
import HomePage from "./Pages/Website/HomePage/";
import { Route, Routes } from "react-router-dom";
import { useScrollToTop } from "./CustomHooks/useScrollToTop";
import AboutUs from "./Pages/Website/AboutUs/";
import Error404 from "./Pages/Error404/";
import ContactUs from "./Pages/Website/ContactUs/";
import Projects from "./Pages/Website/Projects/";
import BlogPost from "./Pages/Website/Blog/BlogPost";
import SinglePost from "./Pages/Website/Blog/SinglePost";
import PostCategory from "./Pages/Website/Blog/PostCategory";


function App() {
  useScrollToTop();
  return (
    <>
      <Routes>
        <Route path="/" exact element={<HomePage />} />
        <Route path="/about-us" element={<AboutUs />} />
        <Route path="/contact-us" element={<ContactUs />} />
        <Route path="/projects" element={<Projects />} />
        <Route path="/blog" element={<BlogPost />} />
        <Route path="/single-post/:id" element={<SinglePost />} />
        <Route path="/post-category/:category" element={<PostCategory />} />
        {/* <Route path="/custom-product/:name" element={<CustomProduct />} /> */}
        {/* 404 Rout */}
        <Route path="/*" element={<Error404 />} />
      </Routes>
    </>
  );
}

export default App;
