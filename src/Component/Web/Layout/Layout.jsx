import React, { useEffect } from "react";
import Footer from "./Footer/Footer";
import NavBar from "./NavBar/NavBar";
import "./scss/layout.css";

export default function Layout({ children, pageTitle }) {
  useEffect(() => {
    document.title = "Pintor Design | " + pageTitle;
  }, [pageTitle]);

  return (
    <>
      <NavBar />
      {children}
      <Footer />
    </>
  );
}
