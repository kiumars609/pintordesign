import React, { version } from "react";
import { Link } from "react-router-dom";

export default function Footer() {
  return (
    <>
      <footer className="ftco-footer ftco-bg-dark ftco-section">
        <div className="container">
          <div className="row mb-5">
            <div className="col-md">
              <div className="ftco-footer-widget mb-4">
                <h2 className="ftco-heading-2 text-center">
                  <img
                    src="/assets/images/icon/logo-white.png"
                    className="col-md-8"
                  />
                </h2>
                <p className="text-right" dir="rtl">
                  شرکت معماری و دکوراسیون داخلی پینتور در سال 1400 در مشهد تاسیس
                  شده است و با کادری مجرب با سابقه درخشان با تکیه بر اصول و
                  مبانی مهندسی در زمینه طراحی و اجرا از صفر تا صد در خدمت شما
                  هموطنان و کار فرمایان عزیز می باشد.
                </p>
                <ul className="ftco-footer-social list-unstyled float-md-right float-lft mt-3">
                  <li className="">
                    <a
                      href="https://instagram.com/pintor.design.official"
                      target="_blank"
                    >
                      <span className="icon-instagram"></span>
                    </a>
                  </li>
                  <li className="">
                    <a
                      href="https://instagram.com/pintor.design"
                      target="_blank"
                    >
                      <span className="icon-instagram"></span>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md text-right">
              <div className="ftco-footer-widget links mb-4 ml-md-4">
                <h2 className="ftco-heading-2">دسترسی سریع</h2>
                <ul className="list-unstyled">
                  <li>
                    <Link to={"/"}>صفحه اصلی</Link>
                  </li>
                  <li>
                    <Link to={"/about-us"}>درباره ما</Link>
                  </li>
                  <li>
                    <a href="#">پروژه های ما</a>
                  </li>
                  <li>
                    <a href="#">خبرگذاری ما</a>
                  </li>
                  <li>
                    <Link to={"/contact-us"}>تماس با ما</Link>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md text-right">
              <div className="ftco-footer-widget links mb-4 ml-md-4">
                <h2 className="ftco-heading-2">آخرین خبرگذاری</h2>
                <ul className="list-unstyled">
                  <li>
                    <a href="#">بازسازی در کوهک</a>
                  </li>
                  <li>
                    <a href="#">بازسازی در ازگل</a>
                  </li>
                  <li>
                    <a href="#">طراحی داخلی در شهرک گلستان</a>
                  </li>
                  <li>
                    <a href="#">طراحی داخلی در تهرانسر</a>
                  </li>
                  <li>
                    <a href="#">بازسازی در چیذر</a>
                  </li>
                </ul>
              </div>
            </div>
            <div className="col-md text-right">
              <div className="ftco-footer-widget contact mb-4">
                <h2 className="ftco-heading-2">تماس با ما</h2>
                <ul className="list-unstyled">
                  <li>
                    <span className="title">آدرس: </span>
                    <span className="text">
                      مشهد، بلوار جلال آل احمر، جلال آل احمر 20، پلاک 20، واحد 3
                    </span>
                  </li>
                  <li>
                    <span className="title">تلفن: </span>
                    <span className="text">
                      <a href="tel:+989157080918">09157080918</a>
                    </span>
                  </li>
                  <li>
                    <span className="title">تلفن: </span>
                    <span className="text">
                      <a href="tel:+989151593897">09151593897</a>
                    </span>
                  </li>
                  <li>
                    <span className="title">ایمیل: </span>
                    <span className="text">info@pintor.com</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 text-center">
              <p>
                {/* <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --> */}
                Copyright &copy; 2023 Pintor Design
                <script>document.write(new Date().getFullYear());</script> All
                rights reserved
                <br />
                <span>تمام حقوق این سایت متعلق به پینتور دیزاین می باشد.</span>
                {/* <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --> */}
              </p>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}
