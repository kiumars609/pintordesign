import React from "react";
import "./scss/NavBar.css";
import { Link, useLocation } from "react-router-dom";
import ScrollAnimation from "react-animate-on-scroll";
import "animate.css/animate.min.css";

export default function NavBar() {
  const location = useLocation();

  const navLinks = [
    { name: "خانه", link: "/" },
    { name: "درباره ما", link: "/about-us" },
    { name: "پروژه های ما", link: "/projects" },
    { name: "خبرگذاری ما", link: "/blog" },
    { name: "تماس با ما", link: "/contact-us" },
  ];

  const queryNavLinks =
    navLinks &&
    navLinks.map((item, index) => {
      return (
        <li
          className={`nav-item ${location.pathname === item.link && "active"}`}
          key={index}
        >
          <Link to={item.link} className="nav-link">
            {item.name}
          </Link>
        </li>
      );
    });

  return (
    <>
      <nav
        className="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light"
        id="ftco-navbar"
      >
        <div className="container col-md-12">
          <div className="col-md-6 d-flex">
            <Link to={"/"} className="navbar-brand">
              <img
                src="/assets/images/icon/logo.png"
                alt="pintor"
                className="col-md-8 col-10"
              />
            </Link>

            <button
              className="navbar-toggler"
              type="button"
              data-toggle="collapse"
              data-target="#ftco-nav"
              aria-controls="ftco-nav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span className="oi oi-menu"></span>
            </button>
          </div>

          <div className="collapse navbar-collapse" id="ftco-nav">
            <ul className="navbar-nav ml-auto">{queryNavLinks}</ul>
          </div>
        </div>
      </nav>
    </>
  );
}
