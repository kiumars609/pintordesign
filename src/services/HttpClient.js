import axios from 'axios'

axios.defaults.baseURL = process.env.REACT_APP_BASE_URL
// axios.defaults.headers.common['Authorization'] = 555


export function get(url, config = {}) {
    return axios.get(url, config)
        .then(response => response.data)
        .catch(error => console.log(error))
}

export function post(url, data, config = {}) {
    return axios.post(url, data, config)
        .then(response => response.data)
        .catch(error => console.log(error))
}

export function del(url, config = {}) {
    return axios.delete(url, config)
        .then(response => response.data)
        .catch(error => console.log(error))
}

export function put(url, data, config = {}) {
    return axios.put(url, data, config)
        .then(response => response.data)
        .catch(error => console.log(error))
}
